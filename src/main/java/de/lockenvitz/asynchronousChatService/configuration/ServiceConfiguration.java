package de.lockenvitz.asynchronousChatService.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.Executor;

@Configuration
public class ServiceConfiguration
{
    @Bean(name = "asynchronousExecutor")
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(100);
        executor.setThreadNamePrefix("chatSrvThread-");
        executor.initialize();
        return executor;
    }

    @Bean
    public RestTemplate createRestTemplate() {
        return new RestTemplate();
    }
}
