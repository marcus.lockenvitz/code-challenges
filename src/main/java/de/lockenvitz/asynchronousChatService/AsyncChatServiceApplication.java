package de.lockenvitz.asynchronousChatService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class AsyncChatServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AsyncChatServiceApplication.class, args);
    }
}
