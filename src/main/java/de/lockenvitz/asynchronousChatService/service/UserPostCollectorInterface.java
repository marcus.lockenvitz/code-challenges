package de.lockenvitz.asynchronousChatService.service;

import de.lockenvitz.asynchronousChatService.model.UserPost;

import java.util.List;

public interface UserPostCollectorInterface {

    List<UserPost> collectUserPost(final int passingMaxCount);
}
