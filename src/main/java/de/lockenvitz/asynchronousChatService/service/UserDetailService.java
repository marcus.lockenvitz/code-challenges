package de.lockenvitz.asynchronousChatService.service;

import de.lockenvitz.asynchronousChatService.model.Post;
import de.lockenvitz.asynchronousChatService.model.User;
import de.lockenvitz.asynchronousChatService.model.UserPost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class UserDetailService implements UserPostCollectorInterface {

    private static final Logger logger = LoggerFactory.getLogger(UserDetailService.class);

    private final UserService userService;
    private final PostService postService;

    public UserDetailService(UserService userService, PostService postService) {
        this.userService = userService;
        this.postService = postService;
    }

    @Override
    public List<UserPost> collectUserPost(final int passingMaxCount) {

        logger.info("#### Info: Start to collect user and post data");

        final List<UserPost> userPostList = new ArrayList<>();
        final long start = System.currentTimeMillis();

        //todo: Improvement: Instead of for loop use java lambda expressions for better code quality
        //todo: Improvement: avoid to set the passingMaxCount manually. There should be a better way.
        for (int i = 1; i <= passingMaxCount; i++) {
            final CompletableFuture<User> completableUser = userService.collectUserData(i);
            final CompletableFuture<List<Post>> completablePosts = postService.collectPostData(i);
            try {
                CompletableFuture.allOf(completableUser, completablePosts).join();
                userPostList.add(new UserPost(completableUser.get(), completablePosts.get()));
            } catch (NullPointerException | ExecutionException | InterruptedException e) {
                //todo: Improvement: should be tested. Hint: write own ExceptionHandler, throw Exception and test it
                logger.error("#### ERROR: Could not collect user or post date: \n" + e.getMessage());
                e.printStackTrace();
            }
        }

        logger.info("#### Info: Collecting data complete");
        logger.info("# Duration: " + (System.currentTimeMillis() - start));

        return userPostList;
    }
}
