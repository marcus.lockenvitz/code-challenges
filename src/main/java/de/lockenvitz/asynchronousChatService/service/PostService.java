package de.lockenvitz.asynchronousChatService.service;

import de.lockenvitz.asynchronousChatService.model.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class PostService {

    private static final Logger logger = LoggerFactory.getLogger(PostService.class);
    private static final String POST_API_URL = "http://jsonplaceholder.typicode.com/posts?userId=%d";

    private final RestTemplate restTemplate;

    public PostService(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Async("asynchronousExecutor")
    public CompletableFuture<List<Post>> collectPostData(final int userId) {
        logger.info("# collecting (id: " + userId + ") post data");

        final String url = String.format(POST_API_URL, userId);
        final ResponseEntity<List<Post>> response = restTemplate.exchange(
                url, HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                }
        );
        return CompletableFuture.completedFuture(response.getBody());

    }
}
