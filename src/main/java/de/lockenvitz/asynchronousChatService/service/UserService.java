package de.lockenvitz.asynchronousChatService.service;

import de.lockenvitz.asynchronousChatService.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class UserService {

    private static final Logger logger = LoggerFactory.getLogger(PostService.class);
    private static final String USER_API_URL = "http://jsonplaceholder.typicode.com/users/%d";

    private final RestTemplate restTemplate;

    public UserService(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Async("asynchronousExecutor")
    public CompletableFuture<User> collectUserData(final int userId) {
        logger.info("# collecting (id: " + userId + ") user data");

        final String url = String.format(USER_API_URL, userId);
        final ResponseEntity<User> result = restTemplate.getForEntity(url, User.class);
        return CompletableFuture.completedFuture(result.getBody());
    }
}
